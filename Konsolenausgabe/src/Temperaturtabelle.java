
public class Temperaturtabelle {

	public static void main(String[] args) {
		
		
		//Aufgabe1 sterne anordnen
		System.out.println("Aufgabe 1");
		
		//initialisieren der variabeln
		String stern = "*";
		
		//Anordnung erstellen
		
		System.out.printf("\n%4s%s\n", stern, stern);
		System.out.printf("%s\t%s\n", stern, stern);
		System.out.printf("%s\t%s\n", stern, stern);
		System.out.printf("%4s%s\n\n", stern, stern);
		
		
		//Aufgabe2 Fakultät berechnen und ausgeben
		System.out.println("Aufgabe 2");
		
		
		System.out.printf("\n%d%s%5s%21s%4d\n", 0, "!", "=", "=", 1);
		System.out.printf("\n%d%s%5s\t%d%19s%4d\n", 1, "!", "=", 1, "=", 1);
		System.out.printf("\n%d%s%5s\t%d%2s%2d%15s%4d\n", 2, "!", "=", 1, "*", 2,  "=", 2);
		System.out.printf("\n%d%s%5s\t%d%2s%2d%2s%2d%11s%4d\n", 3, "!", "=", 1, "*", 2, "*", 3,  "=", 6);
		System.out.printf("\n%d%s%5s\t%d%2s%2d%2s%2d%2s%2d%7s%4d\n", 4, "!", "=", 1, "*", 2, "*", 3, "*", 4,  "=", 24);
		System.out.printf("\n%d%s%5s\t%d%2s%2d%2s%2d%2s%2d%2s%2d%3s%4d\n\n", 5, "!", "=", 1, "*", 2, "*", 3, "*", 4, "*", 5,  "=", 120);
		
		
		
		
		
		
		
		//Aufgabe3 Temperaturen-Tabelle
		System.out.println("Aufgabe 3");
		
		//initialisieren der variabeln
		String fahrenheit = "Fahrenheit";
		String celsius = "Celsius";
		String striche = "------------------------";
		
		//erstellen der Tabelle mit printf
		System.out.printf("\n%-12s|%10s\n", fahrenheit, celsius);
		System.out.printf("%s\n", striche);
		System.out.printf("%-12d|%10.2f\n", -20, -28.8889);
		System.out.printf("%-12d|%10.2f\n", -10, -23.3333);
		System.out.printf("%+-12d|%10.2f\n", 0, -17.7778);
		System.out.printf("%+-12d|%10.2f\n", 20, -6.6667);
		System.out.printf("%+-12d|%10.2f\n", 30, -1.1111);
	}

}
