
public class Aufgabe1 {

	
	
	public static void main(String[] args) {

		String satz1 = "Hallo mein Name ist Tobi. ";
		String satz2 = "heute ist Freitag und bald Wochenende.\n";
		String stern = "*************";
		
		//Aufgabe 1 Zwei Sätze in einer Zeile mit zwei Befehlen
		System.out.print(satz1);
		System.out.print(satz2);
		
		System.out.printf("\"Hallo\"mein Name ist Tobi. \n");
		System.out.println("Ich freue mich, weil " + satz2 + "Morgen spielt Hertha");
		
		//println setzt automatisch am ende des strings einen zeilenumbruch, print schreibt immer in eine zeile
		
		//Aufgabe 2 Muster
		System.out.printf("%10.1s\n", stern);
		System.out.printf("%11.3s\n", stern);
		System.out.printf("%12.5s\n", stern);
		System.out.printf("%13.7s\n", stern);
		System.out.printf("%14.9s\n", stern);
		System.out.printf("%15.11s\n", stern);
		System.out.printf("%16s\n", stern);
		System.out.printf("%11.3s\n", stern);
		System.out.printf("%11.3s\n", stern);
		
		
		
		//Aufgabe 3 Zahlen manipulieren
		double zahl1 = 22.4234234;
		double zahl2 = 111.2222;
		double zahl3 = 4.0;
		double zahl4 = 41000000.551;
		double zahl5 = 97.34;
		System.out.println("Aufgabe3");
		System.out.printf("%.2f\n%.2f\n%.2f\n%.2f\n%.2f", zahl1, zahl2, zahl3, zahl4, zahl5);
		
		int a =2, b =3;
		double x = 2, y = 11, z = 2;
		
		System.out.println("\nhier");
		System.out.println(x++);
		System.out.println(++x);
		x = 1;
		x += y;
		System.out.println (x);
		System.out.println (-y - +y);
		System.out.println(y/x);
		x = 4;
		System.out.println (y%x);
		System.out.println (y%- -z);
		System.out.println (y+++x++);
		//System.out.println(a<b);
	}

}
