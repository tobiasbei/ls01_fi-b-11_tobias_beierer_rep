import java.util.Scanner;

public class Matrix {

	public static void main(String[] args) {
		Scanner myScanner = new Scanner(System.in);
		int i = 0;
		System.out.print("Bitte geben sie eine Zahl zwischen 2 und 9 ein: ");
		
		
		int number = readNumber(myScanner);
		System.out.println("Ihre Eingabe ist: " + number);
		System.out.println();
		while(!checkCorrectness(number)) {
			number = readNumber(myScanner);
		}
		
		 
		do {
			
			int check = number;
			//umbruch vor allen zehnern
		    if(i % 10 == 0) {
			  System.out.println();
			}
		    
		    //regeln prüfen und im falle sternchen ausgeben
		    if(check == i || i%check == 0 || Quersumme.quersumme(i) == check || check == i%10 || check == i/10){
		    	System.out.print(" * ");
		    }
		    else{ //falls nicht zahl ausgeben
		    	
		    	System.out.print(" " + i + " ");
		    }
		    i++;
		}
		while( i<100);

		    
		    
	}
	
	public static int readNumber(Scanner myScanner) {
		int number = myScanner.nextInt();
		
		return number;
	}
	
	public static boolean checkCorrectness(int number) {
		if (number < 2 || number > 9) {
			System.out.println("Das war leider keine korrekte Eingabe. Bitte versuchen Sie es erneut.");
			return false;
		}return true;
	}
	
	
	

}
