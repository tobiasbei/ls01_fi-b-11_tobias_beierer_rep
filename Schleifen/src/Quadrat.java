import java.util.Scanner;

public class Quadrat {

	public static void main(String[] args) {
		Scanner myScanner = new Scanner(System.in);
		System.out.println("Bitte geben Sie eine Zahl ein für die Kantenlänge des Quadrats");
		int kantenlaenge = myScanner.nextInt();
		
		for (int i = 0; i < kantenlaenge; i++) {
		    for (int j = 0; j < kantenlaenge; j++) {
		        if (i == 0 || i == kantenlaenge - 1 || j == 0 || j == kantenlaenge - 1) {
		            System.out.print("* ");
		        } else {
		            System.out.print("  ");
		        }
		    }
		    System.out.println();
		}
		
		myScanner.close();
	}

}
