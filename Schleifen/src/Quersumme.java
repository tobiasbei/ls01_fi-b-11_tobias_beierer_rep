import java.util.Scanner;

public class Quersumme {

	public static void main(String[] args) {
		Scanner myScanner = new Scanner(System.in);
		System.out.println("Bitte geben Sie eine Zahl ein für die Berechnung der Querquerquersummememe");
		int zahl = myScanner.nextInt();
		int quersumme = quersumme(zahl);
		

        System.out.println(quersumme); 
        
        myScanner.close();
	}

	
	public static int quersumme(int zahl) {
		int quersumme = 0; 
		while (zahl > 0) {
	            quersumme = quersumme + zahl % 10;
	           // System.out.println(quersumme);
	            zahl = zahl / 10;
	        }
		 return quersumme;
	}
}
