import java.util.Scanner;


/*
 * 4.1 Einführeung Auswahlstrukturen
 * AB Fallunterscheidung Aufgabe 6 Römische Zahlen II
 * 
 * Das Programm besteht aus 3 Methoden. getRomanNumber(), checkCorrectness() und getDecimal().
 * In der Main startet der Scanner und wird der Methode getRomanNumber() mitgegeben. Hier erfolgt das einlesen des Strings.
 * Dieser wird für den Fall in Großbuchstaben umgewandelt.
 * Die while-Schleife in der Main checkt ihn nun auf Korrektheint. Enstpricht die Römische Zahl nicht den Regeln muss
 * neu eingegeben werden. Durch die ganzen if-Abfragen in checkCorrectness() wird zudem automatisch sichergestellt,
 * dass die Zahl nicht größer als 3999 ist.
 * In getDecimal wird abschließend die Dezimalzahl ermittelt. Dies erfolgt via Switch. Innerhalb der Cases wird
 * mit einigen if-Abfragen die richtige Subtraktion sichergestellt.
 * 
 * Der Code funktioniert, aber mit einer geeigneten Regex oder eines Arrays könnte man checkCorrectness()
 * noch optimieren
 */

public class Rom {

	public static void main(String[] args) {
		Scanner myScanner = new Scanner(System.in);
		String r = getRomanNumber(myScanner);
		
		while(checkCorrectness(r) == false) {
			r = getRomanNumber(myScanner);
		}	
		
		
		int decimal = getDecimal(r);
		System.out.println(decimal);
		myScanner.close();
	}
	
	
	public static String getRomanNumber(Scanner myScanner) {
		
		System.out.println("Bitte geben Sie eine römische Zahl ein (besteht aus M, D, C, L, X, V oder I)");
		String r = myScanner.next().toUpperCase();
		
		return r;
	}
	
	
	public static boolean checkCorrectness(String romanNumber) {
		if(romanNumber != null){
			if(!romanNumber.matches("[MDCLXVI]*")){
				System.out.println("Das waren leider keine oder nicht nur römische Zeichen. Versuchen Sie es erneut.");
				return false;
			}else if (romanNumber.length() > 1) {
				if (romanNumber.contains("MMMM") || romanNumber.contains("CCCC") || 
						romanNumber.contains("XXXX") || romanNumber.contains("IIII") || 
						romanNumber.contains("VV") || romanNumber.contains("LL") || 
						romanNumber.contains("DD")){
					System.out.println("Ihre Eingabe war leider nicht korrekt! M, C, X und I dürfen maximal"
							+ "dreimal aufeinander folgen und V, L und D dürfen nicht mehrfach"
							+ "aufeinander folgen. Bitte geben Sie eine neue Zahl ein.");
					return false;
				}else if(romanNumber.contains("IIIV") || romanNumber.contains("IIV") || 
						romanNumber.contains("IIIX") || romanNumber.contains("IIX") || 
						romanNumber.contains("XXXL") || romanNumber.contains("XXL") ||
						romanNumber.contains("XXXC") || romanNumber.contains("XXC") ||
						romanNumber.contains("CCCD") || romanNumber.contains("CCD") ||
						romanNumber.contains("CCCM") || romanNumber.contains("CCM")) {
					System.out.println("Es darf immer nur ein I zur Subtraktion vorangestellt werden, bitte geben sie eine neue Zahl ein");
					return false;
				}return true;
			}return true;	
		}
		System.out.println("Sie haben leider nichts eingegeben. Versuchen Sie es erneut.");
		return false;
	}
	
	
	public static int getDecimal(String romanNumber) {
		int helper1 = 0;
		int helper2 = 0;
		int decimal = 0;
		
		//durch die Bedingung der Vorschleife und die variable i wird eine rückwärts durchlaufen des Strings romanNumber möglich
		for(int i = romanNumber.length()-1; i >= 0; i--) {
			
			//helper1 variable passt sich i an damit es kein IndexOutOfBounds gibt
			if(i>0) {
				helper1 = i-1;	
			}
			else {
				helper1 = 0;
			}
			
			if(i == romanNumber.length() - 1){
				helper2 = romanNumber.length()-1;
			}
			else {
				helper2 = i+1;
			}
			
			//zu untersuchendes Zeichen wird gewählt
			char romanLetter = romanNumber.charAt(i);

			//switch mit dem Zeichen
			switch(romanLetter) {
			//cases matchen die möglichen Römischen Zeichen und beachten dabei wenn nötig subtraktion
			//durch if-abfrage des direkte Vorgängers
			case 'M':
				if(romanNumber.charAt(helper1) == 'C') {
					decimal += 900;
					break;
				}
				decimal += 1000;
				break;
				
			case 'D':
				if(romanNumber.charAt(helper1) == 'C') {
					decimal += 400;
					break;
				}
				decimal += 500;
				break;
			
			//durch if-abfrage des direkte vorängers, werden Zeichen, die zur Subtraktion genutzt wurden, ignoriert
			case 'C':
				if(romanNumber.charAt(helper2) == 'M' || romanNumber.charAt(helper2) == 'D') {
					System.out.println("ich bin hier");
					break;
				}else if(romanNumber.charAt(helper1) == 'X') {
					decimal += 90;
					break;
				}
				decimal += 100;
				break;
				
			case 'L':
				if(romanNumber.charAt(helper1) == 'X') {
					decimal += 40;
					break;
				}
				decimal += 50;
				break;
				
			case 'X':
				if(romanNumber.charAt(helper2) == 'C' || romanNumber.charAt(helper2) == 'L') {
					break;
				}else if(romanNumber.charAt(helper1) == 'I') {
					decimal += 9;
					break;
				}
				decimal += 10;
				break;
				
			case 'V':
				if(romanNumber.charAt(helper1) == 'I') {
					decimal += 4;
					break;
				}
				decimal += 5;
				break;
				
			case 'I':
				if(romanNumber.charAt(helper2) == 'X' || romanNumber.charAt(helper2) == 'V') {
					break;
				}
				decimal += 1;
				break;
			}
		}
		return decimal;
	}
}
