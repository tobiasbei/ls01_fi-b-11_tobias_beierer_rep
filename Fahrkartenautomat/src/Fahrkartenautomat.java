﻿import java.util.Scanner;

class Fahrkartenautomat {

	public static void main(String[] args) {

		/*
		 * A4.5: Erster Ansatz für die Endlosschleifen. Aktuell noch nicht optimal, da
		 * es Schleife immer eine Abbuchbedingung haben sollte.
		 * 
		 * Update: Endlosschleife wieder entfernt. Über die Methode anotherOne() wird
		 * der User jetzt gefragt, ob er weitere Tickets kaufen will. Bei "y" geht es
		 * weiter bei "n" wird abgebrochen.
		 */

		String test = "y";
		while ("y".equals(test)) {
			double zuZahlenderBetrag = fahrkartenbestellungErfassen();
			double rueckgabeBetrag = fahrkartenBezahlen(zuZahlenderBetrag);
			fahrscheinAusgeben(rueckgabeBetrag);
			test = anotherOne();
		}
	}

	/*
	 * A4.7: für die Merfachauswahl von Ticketarten erschien es mir sinnvoll eine
	 * extra Methode zu erstellen.
	 */
	public static int fahrkartenArt(int[] auswahlnummer, String[] fahrkartenBezeichnung, double[] einzelpreis) {

		System.out.println("Wählen Sie Ihre Wunschfahrkarte für Berlin AB aus:\n" + "\n"
				+ "Auswahlnummer \t Fahrkartenbezeichnung \t\t Einzelpreis" + "\n" + auswahlnummer[1] + "\t\t"
				+ fahrkartenBezeichnung[1] + "\t\t" + einzelpreis[1] + "\n" + auswahlnummer[2] + "\t\t"
				+ fahrkartenBezeichnung[2] + "\t\t" + einzelpreis[2] + "\n" + auswahlnummer[3] + "\t\t"
				+ fahrkartenBezeichnung[3] + "\t\t" + einzelpreis[3] + "\n" + auswahlnummer[4] + "\t\t"
				+ fahrkartenBezeichnung[4] + "\t\t\t\t" + einzelpreis[4] + "\n" + auswahlnummer[5] + "\t\t"
				+ fahrkartenBezeichnung[5] + "\t\t\t" + einzelpreis[5] + "\n" + auswahlnummer[6] + "\t\t"
				+ fahrkartenBezeichnung[6] + "\t\t\t" + einzelpreis[6] + "\n" + auswahlnummer[7] + "\t\t"
				+ fahrkartenBezeichnung[7] + "\t\t\t" + einzelpreis[7] + "\n" + auswahlnummer[8] + "\t\t"
				+ fahrkartenBezeichnung[8] + "\t" + einzelpreis[8] + "\n" + auswahlnummer[9] + "\t\t"
				+ fahrkartenBezeichnung[9] + "\t" + einzelpreis[9] + "\n" + auswahlnummer[10] + "\t\t"
				+ fahrkartenBezeichnung[10] + "\t" + einzelpreis[10] + "\n\nDrücken Sie die 0 zum Bezahlen");

		Scanner myScanner = new Scanner(System.in);

		int ticketArt = myScanner.nextInt();
		if (ticketArt > auswahlnummer.length - 1) {
			System.out.println("Dieses Ticket gibts es nicht. Bitte wählen Sie ein anderes.");
			ticketArt = fahrkartenArt(auswahlnummer, fahrkartenBezeichnung, einzelpreis);
		}
		return ticketArt;
	}

	/*
	 * A4.5: Die Methode fragt nun nach der Ticket-Art und berechnet dann je nach
	 * Art in einem Switch-case den Ticketpreis.
	 * 
	 * A4.7: innerhalb der neuen while-Schleife wird solange nach einer neuen Art
	 * gefragt bis die Eingabe 9 erscheint.
	 * 
	 * A5.3: enthält Array, switch-case zur Berechnung nicht mehr nötig da über
	 * Index im Array der Preis schneller ermittelt wird. Abbruchbedingung ist nun
	 * nicht mehr die 9, sondern die 0. Dadurch passen die Array-Indizes zu der
	 * Tabelle mit allen Ticketarten. Statt einer while-Schleife nutze ich nun eine
	 * do-while-Schleife
	 */
	
	public static double fahrkartenbestellungErfassen() {

		int[] auswahlnummer = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
		String[] fahrkartenBezeichnung = { "Bezahlen", "Einzelfahrschein Berlin AB", "Einzelfahrschein Berlin BC",
				"Einzelfahrschein Berlin ABC", "Kurzstrecke", "Tageskarte Berlin AB", "Tageskarte Berlin BC",
				"Tageskarte Berlin ABC", "Kleingruppen-Tageskarte Berlin AB", "Kleingruppen-Tageskarte Berlin BC",
				"Kleingruppen-Tageskarte Berlin ABC" };
		double[] einzelpreis = { 0, 2.90, 3.30, 3.60, 1.90, 8.60, 9.00, 9.60, 23.50, 24.50, 24.90 };

		Scanner myScanner = new Scanner(System.in);
		double zuZahlenderBetrag = 0;
		int ticketArt = fahrkartenArt(auswahlnummer, fahrkartenBezeichnung, einzelpreis);
		;
		double anzahlTickets = 0;

		do {
			System.out.print("Anzahl der Tickets (1-10):");
			anzahlTickets = myScanner.nextDouble();

			/*
			 * A4.2: If-Schleife eingefügt, um Ticketbegrenzung auf 1-10 und den Ticketpreis
			 * auf >0 festzulegen. Die Berechnung des Gesamtpreises in der Variable
			 * "zuZahlenderBetrag" findet nur bei Nicht-Erfüllung aller Bedingung statt.
			 * Ansonsten wird der Kunde auf die falsche Eingabe hingewiesen und zur erneuten
			 * Eingabe aufgefordert.
			 * 
			 * Auskommentiert finden Sie auch Alternativ die Forderung der Aufgabenstellung
			 * mit einem Standardwert von 1 weiter zu arbeiten.
			 * 
			 */
			if (anzahlTickets < 1.0 || anzahlTickets > 10.0) {
				System.out.println("Bitte geben Sie Ticketanzahl ein, die zwischen 1 und 10 liegt");
				zuZahlenderBetrag = fahrkartenbestellungErfassen();
			} else {
				zuZahlenderBetrag += einzelpreis[ticketArt] * anzahlTickets;
				System.out.println("Zwischensumme beträgt:" + zuZahlenderBetrag);

				// switch case bevor mit arrays gearbeitet wurde
				/*
				 * switch(ticketArt) { case 1: //zuZahlenderBetrag = 2.90 * anzahlTickets;
				 * zuZahlenderBetrag += 2.90 * anzahlTickets;
				 * System.out.println("Zwischensumme beträgt:" + zuZahlenderBetrag); //ticketArt
				 * = fahrkartenArt(); break; case 2: //zuZahlenderBetrag = 8.60 * anzahlTickets;
				 * zuZahlenderBetrag += 8.60 * anzahlTickets;
				 * System.out.println("Zwischensumme beträgt:" + zuZahlenderBetrag); //ticketArt
				 * = fahrkartenArt(); break; case 3: //zuZahlenderBetrag = 23.50 *
				 * anzahlTickets; zuZahlenderBetrag += 23.50 * anzahlTickets;
				 * System.out.println("Zwischensumme beträgt:" + zuZahlenderBetrag); //ticketArt
				 * = fahrkartenArt(); break; }
				 */
			}
			ticketArt = fahrkartenArt(auswahlnummer, fahrkartenBezeichnung, einzelpreis);
		} while (ticketArt != 0);
		return zuZahlenderBetrag;
	}

	public static double fahrkartenBezahlen(double zuZahlenderBetrag) {
		double eingezahlterGesamtbetrag = 0.0;
		Scanner myScanner = new Scanner(System.in);
		while (eingezahlterGesamtbetrag < zuZahlenderBetrag) {
			System.out.printf("Noch zu zahlen: %.2f EURO \n", (zuZahlenderBetrag - eingezahlterGesamtbetrag));
			System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");

			double eingeworfeneMünze = myScanner.nextDouble();

			if (eingeworfeneMünze <= 2) {
				eingezahlterGesamtbetrag += eingeworfeneMünze;
			} else {
				System.out.println("Der Betrag wurde nicht ankzeptiert, bitte werfen Sie erneut ein:");
				eingeworfeneMünze = myScanner.nextDouble();
				eingezahlterGesamtbetrag += eingeworfeneMünze;
			}
		}


		double rueckgabeBetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;

		return rueckgabeBetrag;

	}

	public static void fahrscheinAusgeben(double rueckgabeBetrag) {
		System.out.println("\nFahrschein wird ausgegeben");
		warte(10);
		System.out.println("\n\n");
		rueckgeldAusgeben(rueckgabeBetrag);
	}

	public static void rueckgeldAusgeben(double rueckgabeBetrag) {
		String einheit = "EURO";

		if (rueckgabeBetrag > 0.00) {
			System.out.printf("Der rueckgabeBetrag in Höhe von %.2f EURO \n", rueckgabeBetrag);
			System.out.println("wird in folgenden Münzen ausgezahlt:");

			while (rueckgabeBetrag >= 2.0) // 2 EURO-Münzen
			{
				muenzeAusgeben(2, einheit);
				rueckgabeBetrag -= 2.0;
			}
			while (rueckgabeBetrag >= 1.0) // 1 EURO-Münzenx
			{
				muenzeAusgeben(1, einheit);
				rueckgabeBetrag -= 1.0;
			}
			while (rueckgabeBetrag >= 0.5) // 50 CENT-Münzen
			{
				muenzeAusgeben(50, einheit);
				rueckgabeBetrag -= 0.5;
			}
			while (rueckgabeBetrag >= 0.2) // 20 CENT-Münzen
			{
				muenzeAusgeben(20, einheit);
				rueckgabeBetrag -= 0.2;
			}
			while (rueckgabeBetrag >= 0.1) // 10 CENT-Münzen
			{
				muenzeAusgeben(10, einheit);
				rueckgabeBetrag -= 0.1;
			}
			while (rueckgabeBetrag >= 0.05)// 5 CENT-Münzen
			{
				muenzeAusgeben(5, einheit);
				rueckgabeBetrag -= 0.05;
			}
		}

		System.out.println("\nVergessen Sie nicht, den Fahrschein\n" + "vor Fahrtantritt entwerten zu lassen!\n"
				+ "Wir wünschen Ihnen eine gute Fahrt.\n");
	}

	public static void warte(int millisekunde) {
		for (int i = 0; i < millisekunde; i++) {
			System.out.print("=");
			try {
				Thread.sleep(250);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	static void muenzeAusgeben(int betrag, String einheit) {

		if (betrag >= 5) {
			einheit = "CENT";
			System.out.println(betrag + " " + einheit);
		} else
			System.out.println(betrag + " " + einheit);
	}

	public static String anotherOne() {
		Scanner myScanner = new Scanner(System.in);
		System.out.println("\nWollen Sie eine weitere Fahrkarte buchen? (y/n)");
		return myScanner.next();
	}
}