/**
  *   Aufgabe:  Recherechieren Sie im Internet !
  * 
  *   Sie dürfen nicht die Namen der Variablen verändern !!!
  *
  *   Vergessen Sie nicht den richtigen Datentyp !!
  *
  *
  * @version 1.0 from 21.08.2019
  * @author << Ihr Name >>
  */

public class WeltDerZahlen {

  public static void main(String[] args) {
    
    /*  *********************************************************
    
         Zuerst werden die Variablen mit den Werten festgelegt!
    
    *********************************************************** */
    // Im Internet gefunden ?
    // Die Anzahl der Planeten in unserem Sonnesystem                    
    byte anzahlPlaneten = 9;
    
    // Anzahl der Sterne in unserer Milchstraße
    long anzahlSterne = 100000000000l;
    
    // Wie viele Einwohner hat Berlin?
    int bewohnerBerlin = 3640000;
    
    // Wie alt bist du?  Wie viele Tage sind das?
    
    short alterTage = 10794;
    
    // Wie viel wiegt das schwerste Tier der Welt?
    // Schreiben Sie das Gewicht in Kilogramm auf!
    int gewichtKilogramm = 150000;  
    
    // Schreiben Sie auf, wie viele km² das größte Land er Erde hat?
    int flaecheGroessteLand = 17100000;
    
    // Wie groß ist das kleinste Land der Erde?
    
    float flaecheKleinsteLand = 0.44f;
    
    
    
    
    /*  *********************************************************
    
         Programmieren Sie jetzt die Ausgaben der Werte in den Variablen
    
    *********************************************************** */
    
    System.out.println("Anzhahl der Planeten: " + anzahlPlaneten);
    
    System.out.println("Anzahl der Sterne: " + anzahlSterne);
    
    System.out.println("Anzahl der Einwohner Berlins: " + bewohnerBerlin);
    
    System.out.println("So alt bin ich in Tagen: " + alterTage);
    
    System.out.println("Das schwerste Tier wiegt: " + gewichtKilogramm);
    
    System.out.println("Das größte Land ist: " + flaecheGroessteLand + "km groß");
    
    System.out.println("Das kleinste Land ist: " + flaecheKleinsteLand + "km groß");
    
    
    System.out.println(" *******  Ende des Programms  ******* ");
    
  }
}
