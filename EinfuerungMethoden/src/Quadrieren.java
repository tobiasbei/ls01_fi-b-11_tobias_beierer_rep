import java.util.Scanner;

public class Quadrieren {

	public static void main(String[] args) {

		title();
		double x = eingabe();
		double ergebnis= verarbeitung(x);
		ausgabe(x, ergebnis);

	}
	
	//Titel
	public static void title() {
		System.out.println("Dieses Programm berechnet die Quadratzahl x^2");
		System.out.println("---------------------------------------------");
	}
	

	// (E) "Eingabe"
	// Wert f�r x festlegen:
	// ===========================
	public static double eingabe() {
		System.out.print("Bitte geben Sie eine Zahl ein:");
		Scanner myScanner = new Scanner(System.in);
		double eingabe = myScanner.nextDouble();
		myScanner.close();
		return eingabe;
	}
	
	
// (V) Verarbeitung
// Mittelwert von x und y berechnen:
// ================================
	public static double verarbeitung(double x) {
		double ergebnis= x * x;
		return ergebnis;
	}
	
	
	// (A) Ausgabe
	// Ergebnis auf der Konsole ausgeben:
	// =================================
	public static void ausgabe(double x, double ergebnis) {
		System.out.printf("\nx = %.2f und x^2= %.2f\n", x, ergebnis);
	}
}
