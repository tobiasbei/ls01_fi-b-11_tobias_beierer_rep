import java.util.Scanner;

//A3.2 AB fuer Fortgeschrittene
public class MethodenProgrammieruebungen {

	public static void main(String[] args) {
		
		// 1. Aufgabe: Berechnung Mittelwert (siehe Klasse Mittelwert.java aus Commit zur A3.1)
		//Mittelwert.berechneMittelwert(2, 4);
		
/********************************************************************************************************************/
		
		//2. Aufgabe: Kassenbon
		
		Scanner myScanner = new Scanner(System.in);

		// Benutzereingaben lesen
		
		String artikel = liesString(myScanner);
		int anzahl = liesInt(myScanner);
		System.out.println("Geben Sie den Nettopreis ein:");
		double preis = liesDouble(myScanner);
		System.out.println("Geben Sie den Mehrwertsteuersatz in Prozent ein:");
		double mwst = liesDouble(myScanner);

		// Verarbeiten
		
		double netto = berechneGesamtnettopreis(anzahl, preis);
		double brutto = berechneGesamtbruttopreis(netto, mwst);

		// Ausgeben

		rechnungAusgeben(artikel, anzahl, netto, brutto, mwst);
		
		
		/********************************************************************************************************************/
		
		//3. Aufgabe: Reihenschaltung
		double x = 2.70;
		double y = 3.60;
		double z = berechnungReihe(x, y);
		System.out.println("Der Widerstand beträgt" + z);
		
		/********************************************************************************************************************/
		
		//4. Aufgabe: Parallelschaltung
		double a = 2.70;
		double b = 3.60;
		double c = berechnungParallel(a, b);
		System.out.println("Der Widerstand beträgt" + c);
		
		/********************************************************************************************************************/
		
		//5. Aufgabe: Quadrieren (siehe Klasse Quadrieren.java aus Commit zur A3.1)
		double q = Quadrieren.verarbeitung(2);
		System.out.println(q);
		
	}
	
	//2. Aufgabe: Kassenbon ---> Methoden
	//Mehode zum Einlesen des Strings
	public static String liesString(Scanner input) {
		System.out.println("was m�chten Sie bestellen?");
		String artikel = input.next();
		return artikel;
	}
	
	//Methode zum Einlesen der Anzahl
	 public static int liesInt(Scanner input) {
		System.out.println("Geben Sie die Anzahl ein:");
		int anzahl = input.nextInt();
		return anzahl;
	 }
	 
	//Methode zum Einlesen des Preises
	 public static double liesDouble(Scanner input) {
		double preis = input.nextDouble();
		return preis; 
	 }
	 
	//Methode zum Berechnen des Nettopreises
	 public static double berechneGesamtnettopreis(int anzahl, double nettopreis) {
		double nettogesamtpreis = anzahl * nettopreis;
		return nettogesamtpreis; 
	 }
	 
	//Methode zum Berechnen des Bruttopreises
	 public static double berechneGesamtbruttopreis(double nettogesamtpreis, double mwst) {
		double bruttogesamtpreis = nettogesamtpreis * (1 + mwst / 100);
		return bruttogesamtpreis; 
	 }
	//Methode zur Ausgabe des Bons
	 public static void rechnungAusgeben(String artikel, int anzahl, double netto, double brutto, double mwst) {
			System.out.println("\tRechnung");
			System.out.printf("Netto:  %-20s %6d %10.2f %n", artikel, anzahl, netto);
			System.out.printf("Brutto: %-20s %6d %10.2f (%.1f%s)%n", artikel, anzahl, brutto, mwst, "%");
	 }

	 
	 /********************************************************************************************************************/
	 //3. Aufgabe: Reihenschaltung ----> Methode
	 public static double berechnungReihe(double x, double y) {
		 double widerstand = x+y;
		 return widerstand;
	 }
	 
	 /********************************************************************************************************************/
	 //4. Aufgabe: Parallelschaltung ----> Methode
	 public static double berechnungParallel(double x, double y) {
		 double widerstand = (x*y)/(x+y);
		 return widerstand;
	 }
}
