import java.util.Scanner;

public class MultiplikationKlausur {

    public static void main(String[] args) {
        
        double zahl1 = 0.0, zahl2 = 0.0, erg = 0.0;
        //1.Programmhinweis
        programmHinweis();
        
        //4.Eingabe
        zahl1 = eingabe("1. Zahl: ");
        zahl2 = eingabe("2. Zahl: ");

        //3.Verarbeitung
        erg = verarbeitung(zahl1, zahl2);

        //2.Ausgabe    
        ausgabe(zahl1, zahl2, erg);
        
    }
    
    public static void programmHinweis() {
    	System.out.println("Hinweis: ");
        System.out.println("Das Programm multipliziert 2 eingegebene Zahlen. ");
    }
    
    public static double eingabe(String text) {
    	Scanner myScanner = new Scanner(System.in);
        System.out.print(text);
        double zahl = myScanner.nextDouble();
    	return zahl;
    }
    
    public static double verarbeitung(double zahl1, double zahl2) {
    	double erg = zahl1 * zahl2;
    	return erg;
    }
    
    public static void ausgabe(double zahl1, double zahl2, double erg) {
        System.out.println("Ergebnis der Multiplikation: ");
        System.out.printf("%.2f * %.2f = %.2f%n", zahl1, zahl2, erg);
    }
    

}
