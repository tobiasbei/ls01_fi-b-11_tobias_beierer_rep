import java.util.Scanner;

public class Mittelwert {

	public static void main(String[] args) {
	    double y = 0;
	    double m;
		Scanner myScanner = new Scanner(System.in);
		int a = anzahlFestlegen(myScanner, "Legen Sie die Anzahl fest");
		double [] anzahl = new double[a];
		int i = 0;
		
		
		for(i = 0; i<anzahl.length; i++) {
			double x = zahlenEinlesen(myScanner);
			anzahl[i] = x;
		}
		
		/*
		while (i < a) {
			double x = zahlenEinlesen(myScanner);
			y += x;
			i++;
		}	
		*/
		
		m = berechneMittelwert(anzahl);

		// (A) Ausgabe
	    // Ergebnis auf der Konsole ausgeben:
	    // =================================
		System.out.printf("Der Mittelwertist %.2f\n", m);

	}
	
	
    // (E) "Eingabe"
    // Werte festlegen:
    // ===========================
	
	public static int anzahlFestlegen(Scanner myScanner, String text) {
		System.out.println(text);
		return myScanner.nextInt();
	}
	
	public static double zahlenEinlesen(Scanner myScanner) {
		System.out.println("Geben Sie Zahlen zum Berechnen des Mittelwerts ein");
		double x = myScanner.nextDouble();
		return x;
	}
	
    // (V) Verarbeitung
    // Mittelwert von x und y berechnen: 
    // ================================
	public static double berechneMittelwert(double[] zahlen) {
		double sum = 0;
		
		for(int i = 0; i < zahlen.length; i++) {
			sum += zahlen[i];
		}
		
		return sum / zahlen.length;
	}

}
