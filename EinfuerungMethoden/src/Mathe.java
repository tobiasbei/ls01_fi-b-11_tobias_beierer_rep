import java.util.Scanner;

/*
 * Diese Klasse berchnet nur die hypthenuse, da Quadrieren und CO bereits in eigerner Klasse. Aufruf der Methode 
 * erfolgt deshalb klassenübergreifen
 */
public class Mathe {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner myScanner = new Scanner(System.in);
		System.out.print("Bitte geben Sie zwei Zahlen ein:");
		double k1 = myScanner.nextDouble(), k2= myScanner.nextDouble();
		
		double h = Mathe.hypotenuse(k1, k2);
		System.out.println(h);
		myScanner.close();
	}
	
	
	static double hypotenuse(double kathete1, double kathete2) {
		double h = Math.sqrt(Quadrieren.verarbeitung(kathete1) + Quadrieren.verarbeitung(kathete2));
		return h;
	}

}
