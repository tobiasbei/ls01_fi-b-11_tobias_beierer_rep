import java.util.Scanner;

public class Fahrsimulator {

	public static void main(String[] args) {
		double v = 0.00;
		double dv = 0.00;
		System.out.println("Deine Startgeschwindigkeit beträgt 0,00 km/h.");
		Scanner myScanner = new Scanner(System.in);
		
		//while-Schleife bricht bei 130 ab
		while(v<130) {
			System.out.println("Bitte gibt einen Beschleunigungswert ein, der ungleich 0 ist");
			dv = myScanner.nextDouble();
			
		//if-Abfrage verhindert berechnung und Ausgabe von Werten über 130
			if (beschleunige(v, dv)<=130){
				v = beschleunige(v, dv);
				System.out.printf("Geschwindikeit liegt bei %.2f\n", v);
			} else System.out.println("Du bist zu schnell! Beschleunige nicht so stark!");
		}
		//v = beschleunigeAlternative(v, myScanner);
		System.out.printf("Deine Endgeschwindigkeit beträgt: %.2f", v);
		
		myScanner.close();
	}

	/* Methode wie in der Aufgabenstellung gefordert	
	 */
		public static double beschleunige(double v, double dv) {
		v = v+dv;
		return v;
	}	
	
	/* komplexere Methode mit dem Versuch durch eine for-Schleife die "Zeit" als Einheit zu erhalten und somit die Geschwindigkeit
	 * entsprechend der physikalischen Formel v=a*t+v0 zu berechnen. Zusätzliche Kontrolle, um negative Geschwindigkeit zu verhindern.
	 * Außerdem findet Scanner input und Schleife innerhalb der Methode statt nicht in der Main.
	 */
	public static double beschleunigeAlternative(double v, Scanner input) {
			for (int i = 1; v<130.00; i++) {
				System.out.println("Bitte gibt einen Beschleunigungswert ein, der größer 0 ist");
				double dv = input.nextDouble();
				if((v+dv*i)<=130.00 && (v+dv*i)>0.00) {
				v = v+dv*i;
				System.out.printf("Geschwindikeit liegt bei %.2f\n", v);
				} else if ((v+dv*i)<0.00) {
					System.out.println("Geschwindigkeit kann nicht unter 0 sein. Drück mal aufs Gas!");
				} else System.out.println("Du bist zu schnell! Beschleunige nicht so stark!");
			}
		return v;
	}
}
