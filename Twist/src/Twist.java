import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;


/*
 * Extraaufgabe Twist
 * 
 * Bisheriger Stand: Liest ein Wort ein, wandelt den String in ein char-Array um, bringt das Array durcheinander,
 * prüft ob das neue getwistete Wort doch noch dem alten gleicht und gibt anschließend das getwistete Wort aus.
 * 
 * Satz twisten funktioniert jetzt auch. 
 * Allerdings merkt man stark, dass hier die abfrage, die im einzelnen wort prüft, ob es wieder das gleiche wort ist, fehlt. 
 * Umso kürzer das Wort desto häufiger kommt es vor, dass das wort beim twisten nicht verändert wird. 
 * Das liegt an der Random.Zahl. dafür muss ich mir noch etwas überlegen.
 * 
 * 
 */

public class Twist {

	public static void main(String[] args) {
		char[] buchstabenTwisted = null;
		String[] wordsTwisted = null;

		Scanner myScanner = new Scanner(System.in);
		
		//char[] buchstaben  = new char[wort.length()];

		System.out.println("Wollen Sie ein Wort oder einen Satz twisten? [w/s]");
		char decision = myScanner.next().charAt(0);
		myScanner.nextLine();
		
		switch(decision) {
		case 'w':
			
			String wort = getWord(myScanner);
			char[] buchstaben = getCharsOfWord(wort);
			char[] buchstabenCorrectOrder = Arrays.copyOf(buchstaben, buchstaben.length);
			buchstabenTwisted = twistWord(buchstaben);
			
			//prüft, ob die Buchstaben zufälliger Weise wieder korrekt angeordnet wurden
			if(Arrays.equals(buchstabenTwisted, buchstabenCorrectOrder)) { 
				buchstabenTwisted = twistWord(buchstabenCorrectOrder);
			}
			System.out.println(buchstabenTwisted);
			break;
		case 's':
			String sentence = getSentence(myScanner);
			wordsTwisted = twistSentence(sentence);
			String sentenceTwisted = String.join(" ", wordsTwisted);
			System.out.println(sentenceTwisted);
			break;
		}
		myScanner.close();
	}
	
	
	
	
	public static String getWord(Scanner myScanner) {
		System.out.println("Bitte geben Sie ein Wort ein, das getwisted werden soll");
		String wort = myScanner.next();
		wort.trim();
		return wort;
	}
	
	
	
	public static String getSentence(Scanner myScanner) {
		System.out.println("Bitte geben Sie eien Satz ein, das getwisted werden soll");
		String sentence = myScanner.nextLine();
		return sentence;
	}
	
	
	
	//get char array of the word
	public static char[] getCharsOfWord(String word){
		char[] buchstaben = new char[word.length()];
		for (int i=0; i<word.length(); i++) {
			buchstaben [i] = word.charAt(i);
		}
		return buchstaben;
	}
	
	
	
	//twist a single word (safed in char array)
	public static char[] twistWord(char[] buchstaben) {
		//char [] buchstabenTwisted;
		char tmp1;
		char[] buchstabenTwisted = Arrays.copyOf(buchstaben, buchstaben.length);
		
		
		for(int i=1; i < buchstaben.length-1; i++) {
			int j = new Random().nextInt(buchstaben.length-1);
			if(i < buchstaben.length/2 && j>0) { //ohne buchstabe.length/2 geht die schleife das ganze wort durch und es wird wieder "normal"
				tmp1 = buchstaben[i];
				buchstaben[i] = buchstaben[j];
				buchstaben[j] = tmp1;					
			}
		}
		return buchstaben;
	}
	
	
	
	//Twist a whole sentenct
	public static String[] twistSentence(String sentence) {		
		String [] words = sentence.split("[\\s+]"); // splitte den Satz, wenn Leerzeichen und speichere wörter im String array words
		String[] wordsTwisted = new String[words.length];
		
		for (int i = 0; i<words.length;i++) { 
			char[] buchstaben = getCharsOfWord(words[i]);
			String twistedWord = new String(twistWord(buchstaben));
			//wordsTwisted[i] = twistWord(getCharsOfWord(words[i])).toString();
			wordsTwisted[i] = twistedWord;
		}
		return wordsTwisted;
	}
}