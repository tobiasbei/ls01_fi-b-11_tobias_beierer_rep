
public class Cargo {
	
	private String name;
	private double amount;

	public Cargo() {
	}
	
	
	public Cargo(String name, double amount) {
		this.name = name;
		this.amount = amount;
	}


	
	/*
	 * all the getter and setter
	 */
	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public double getAmount() {
		return amount;
	}


	public void setAmount(double amount) {
		this.amount = amount;
	}

}
