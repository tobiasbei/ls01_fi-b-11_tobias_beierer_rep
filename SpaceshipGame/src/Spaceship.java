import java.util.ArrayList;

public class Spaceship {

	private String name;
	private String captain;
	//private Cargo cargo;
	private double energy;
	private double shield;
	private double shell;
	private double liveSystem;
	private int amountTorpedos;
	private int amountAndroids;
	private ArrayList<String> broadcastCommunicator = new ArrayList<>();
	private ArrayList<Cargo> allCargos;


	
	
	public Spaceship(String name, String captain, double energy, double shield, double shell,
			double liveSystem, int amountTorpedos,int amountAndroids) {
		this.name = name;
		this.captain = captain;
		this.energy = energy;
		this.shield = shield;
		this.shell = shell;
		this.liveSystem = liveSystem;
		this.amountTorpedos = amountTorpedos;
		this.amountAndroids = amountAndroids;
	}
	
	public Spaceship(String name, double energy, double shield, double shell,
			double liveSystem, int amountTorpedos,int amountAndroids) {
		this.name = name;
		this.energy = energy;
		this.shield = shield;
		this.shell = shell;
		this.liveSystem = liveSystem;
		this.amountTorpedos = amountTorpedos;
		this.amountAndroids = amountAndroids;
	}
	
	public Spaceship() {
	}
	
	
	public void addCargo(Cargo cargo) {
		allCargos.add(cargo);
	}
	
	
	
	/*
	 * all the getters and setters
	 */

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	public String getCaptain() {
		return captain;
	}

	public void setCaptain(String captain) {
		this.captain = captain;
	}

	public double getEnergy() {
		return energy;
	}

	public void setEnergy(double energy) {
		this.energy = energy;
	}

	public double getShield() {
		return shield;
	}

	public void setShield(double shield) {
		this.shield = shield;
	}

	public double getShell() {
		return shell;
	}

	public void setShell(double shell) {
		this.shell = shell;
	}

	public int getAmountTorpedos() {
		return amountTorpedos;
	}

	public void setAmountTorpedos(int amountTorpedos) {
		this.amountTorpedos = amountTorpedos;
	}

	public int getAmountAndroids() {
		return amountAndroids;
	}

	public void setAmountAndroids(int amountAndroids) {
		this.amountAndroids = amountAndroids;
	}

	public ArrayList<String> getBroadcastCommunicator() {
		return broadcastCommunicator;
	}

	public void setBroadcastCommunicator(ArrayList<String> broadcastCommunicator) {
		this.broadcastCommunicator = broadcastCommunicator;
	}

	public double getLiveSystem() {
		return liveSystem;
	}

	public void setLiveSystem(double liveSystem) {
		this.liveSystem = liveSystem;
	}

	public ArrayList<Cargo> getAllCargos() {
		return allCargos;
	}

	public void setAllCargos(ArrayList<Cargo> allCargos) {
		this.allCargos = allCargos;
	}
	
	
	

}