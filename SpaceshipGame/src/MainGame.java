
public class MainGame {

	public static void main(String[] args) {
		
		Spaceship klingone = new Spaceship("IKS Heghta", 100.00, 100.00, 100.00, 100.00, 1, 2);
		Spaceship romulanes = new Spaceship("IRW Khazara", 100.00, 100.00, 100.00, 100.00, 2, 2);
		Spaceship vulcanes = new Spaceship("NiVar", 80.00, 80.00, 50.00, 100.00, 0, 5);
		
		
		Cargo c1 = new Cargo("Ferengi Schneckensaft", 200);
		Cargo c2 = new Cargo("Borg Schrott", 2);
		Cargo c3 = new Cargo("Rote Materie", 5);
		Cargo c4 = new Cargo("Forschungssonde", 35);
		Cargo c5 = new Cargo("Barleth Klingonenschwert", 200);
		Cargo c6 = new Cargo("Plasma-Waffen", 50);
		Cargo c7 = new Cargo("Photonentorpedo", 3);
		
		
		klingone.addCargo(c1);
		klingone.addCargo(c5);
		
		romulanes.addCargo(c2);
		romulanes.addCargo(c3);
		romulanes.addCargo(c6);
		
		vulcanes.addCargo(c4);
		vulcanes.addCargo(c7);
		
		
		
	}

}
