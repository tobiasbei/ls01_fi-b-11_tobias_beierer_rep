import java.util.Scanner;

/*
 * 4.1 Einführeung Auswahlstrukturen
 * AB Auswahlstrukturen Aufgabe 5 BMI
 * 
 * Das Program BMI nimmt Gewicht, Größe und Geschlecht durch Konsoleneingabe entgegen. Die Größe wird von cm in m umgerechnet.
 * Anschließend findet die BMI Berechnung statt. Über einen Switch mit folgender if-Abfrage wird die korrekt BMI-
 * Klassifikation ermittelt und ausgegeben.
 * 
 */

public class BMI {

	public static void main(String[] args) {
		Scanner myScanner = new Scanner(System.in);
		int kg;
		double cm;
		char gender;
		
		System.out.println("Bitte geben Sie ihr Gewicht in Kilogramm ein");
		kg = myScanner.nextInt();
		System.out.println("Bitte geben Sie ihre Größe in Centimeter ein");
		cm = myScanner.nextInt();
		double m = cm/100;
		System.out.println("Bitte nennen Sie ihr Geschlecht (m/w)");
		gender = myScanner.next().charAt(0);
		double bmi = berechneBMI(kg, m);
		ausgabeKlassifikation(gender, bmi);
	}
	
	
	public static double berechneBMI(int kg, double m) {
		double bmi = kg/(m*m);
		return bmi;
	}
	
	public static void ausgabeKlassifikation(char gender, double bmi) {
		switch(gender) {
		case 'm':
			if(bmi < 20) {
				System.out.println("Untergewicht");
			}else if(bmi > 25) {
				System.out.println("Übergewicht");
			}else System.out.println("Normalgewicht");
			break;
		case 'w':
			if(bmi < 19) {
				System.out.println("Untergewicht");
			}else if(bmi > 24) {
				System.out.println("Übergewicht");
			}else System.out.println("Normalgewicht");
			break;
		}
	}
	

}
