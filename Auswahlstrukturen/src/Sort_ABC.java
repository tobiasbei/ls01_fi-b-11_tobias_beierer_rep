import java.util.Arrays;
import java.util.Scanner;

/*
 * 4.1 Einführeung Auswahlstrukturen
 * AB Auswahlstrukturen Aufgabe 7 Sortieren
 * 
 * Das Program Sort_ABC akzeptiert Strings und Ints als Eingaben. Es wartet nach der Aufforderung zur Eingabe darauf,
 * dass der User 3 Eingaben tätigt und jede mit Enter bestätigt.
 * Im nächsten Schritt wird geprüft, ob es sich beim ersten Input um einen Int oder String handelt. Je nachdem wird ein
 * Array des Typen befüllt und dieses anschließend aufsteigend sortiert und ausgegeben.
 * 
 * Zur Auswahl stehen jeweils zwei Sortiertverfahren.. die von Java gegebenden Arrays.sort() sowie eine eigene
 * Methode.
 * 
 */

public class Sort_ABC {
	

	public static void main(String[] args) {
		Scanner myScanner = new Scanner(System.in);
		int[] numbers = new int[3];
		String[] letters = new String[3];
		
		
		System.out.println("Bitte geben Sie "+ numbers.length +" Ziffern oder Buchstaben ein");
		
		//eingabe könnte auch leer sein
		if (myScanner.hasNextInt()) {
			for(int i = 0; i < numbers.length; i++) { 	//entsprechend der größe des angelegten arrays wird die for-schleife ausgeführt
				numbers[i] = myScanner.nextInt();
			}
			
			System.out.println(Arrays.toString(numbers));
			
			//einfache Lösung mit der sort-Funktion für Arrays
			//Arrays.sort(numbers);
			
			//komplexere Lösung mit for-Schleife
			sortIntArrays(numbers);
			System.out.println(Arrays.toString(numbers));
			
		}else if (myScanner.hasNext()) {
			for(int i = 0; i < letters.length; i++) {
				letters[i] = myScanner.next();
			}
			
			System.out.println(Arrays.toString(letters));
			
			//einfache Lösung mit der sort-Funktion für Arrays
			//Arrays.sort(letters);
			
			//komplexere Lösung mit for-Schleife
			sortStringArrays(letters);
			System.out.println(Arrays.toString(letters));
		}
		myScanner.close();
	}
	
	
	public static void sortIntArrays(int[]numbers) {
		int tmp;
		
		for(int i = 0; i < numbers.length; i++) {
			for(int j = 0; j < numbers.length; j++) {
				if(numbers[i] < numbers[j]) {
					tmp = numbers[i];
					numbers[i] = numbers[j];
					numbers[j] = tmp;
				}
			}
		}
	}
	
	public static void sortStringArrays(String[]letters) {
		String tmp;
		for(int i = 0; i < letters.length; i++) {
			for(int j = 0; j < letters.length; j++) {
				if(letters[i].compareTo(letters[j]) < 0) {
					tmp = letters[i];
					letters[i] = letters[j];
					letters[j] = tmp;
				}
			}
		}
	}
}
